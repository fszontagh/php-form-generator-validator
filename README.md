# README #

This is a PHP based form generator, for faster development. Easy to integrate with MVC and/or other template systems. 
Usable for small or medium projects.

### What is this repository for? ###

* Easy form generator, more time to the system logic, less time for the standard coding. 

### How do I get set up? ###

* First of, clone the repository into the project folder
* No extra configuration required
* No extra dependencies

For examples, please check out the [form_test.php](https://bitbucket.org/fszontagh/php-form-generator-validator/src/e0d6ebcfd92b1ef0124672b6435fe92918bfc3bd/form_test.php?fileviewer=file-view-default) file.
Live demo: http://fsociety.hu/classess/test/form_generator/form_test.php


If You have questions, please feel free to contact with me at fszontagh(at)fsociety(dot)hu