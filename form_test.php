<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP Form generator2 with Bootstrap CSS</title>

    <!-- Bootstrap -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	  <div class="container">
		  <div class="row">
			  <h1>Form generator with Bootstrap</h1>
<?php
error_reporting(E_ALL);
require_once 'form2.class.php';

$form = new form2();
//create input username
$username = $form->addInput("text","username","Username");
//add attributums to username
$username->addAttr(array("required"=>true,"placeholder"=>"Please specify your username","minlength"=>2));
$username->addAttr("autocomplete","off");

$username->addRule("regex","/([0-9]+)/s","Wrong username format: only numbers allowed :)");

$teszt = $form->addInput("text","test","Test");
$teszt->addAttr(array("placeholder"=>"Please write here the following hungarian word: 'teszt'","minlength"=>5,"maxlength"=>5));
$teszt->addRule("callback","validate_teszt_form","Value is not \"teszt\"");

function validate_teszt_form($param){
	return $param == "teszt";
}

//!!datetime is not supported by all browser!!
$datetime = $form->addInput("datetime-local","DateTime","Birth Date");
$datetime->addAttr("placeholder","Please specify your Birth Date with the following format: YYYY. MM. DD. hh:mm");

$number = $form->addInput("number","age","Your age");
$number->addAttr(array("min"=>5,"max"=>150));

$password = $form->addInput("password","pass","Password");
$password->addAttr(array("autocomplete"=>"off","min-length"=>8,"max-length"=>16,"required"=>true,"placeholder"=>"Min length: 8, max length: 16"));


$email = $form->addInput("email","mail","E-Mail");
$email->addAttr("required",1);
$email->addAttr("placeholder","What's your e-mail address?");


$heroes = array(
	"Batman",
	"Roobin",
	"Arrow",
	"Captain Amercia",
	"Billy The Gates",
	"Superman"
);

$users = $form->addInput("select","users","Users",$heroes);
$users->addAttr("selected",3);

$users->addAttr("style","border: 1px solid red;background-color: #90EE90;color: #000");

$radio1 = $form->addInput("radio","batman","Yes, this is it","yes");
$radio2 = $form->addInput("radio","batman","No, this is not","no");


$radio1->addAttr("required",true);
$radio2->addAttr("required",true);

$checkbox = $form->addInput("checkbox","you_are_crazy","You are a littlebit crazy?","iam_crazy");

$checkbox->addAttr(array("required"=>true));


$button = $form->addButton("save","Mentés");
$resetButton = $form->addResetButton("reset","Reset",true);

//validate before show
// validate methon only validate, when form submitted...
if ($form->validate()===true) {
	echo '<p class="bg-success">Everything is valid!</p>';
}


//echo nl2br(highlight_string ($form->show(),true));
echo "<hr/>";
echo $form->show();
?>
</div>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
  </body>
</html>
